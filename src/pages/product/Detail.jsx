import React, {Component} from 'react';

import {Row, Col, Divider , Tabs , Card} from 'antd';
import TopBar from '../../components/TopBar';
import FooterBar from 'components/FootBar'
import SearchBar from '../../components/SearchBar'
import TopMenu from '../../components/TopMenu';
import ImageGallery from '../../components/ImageGallery';
import AttributesTable from '../../components/AttributesTable';
import RecommendedProducts from './Detail/RecommendedProducts';
import ProductInfo from './Detail/ProductInfo';
import ProductCrumbs from './Detail/ProductCrumbs'
import CommentListView from '../comment/List';
import HttpUtils from "../../utils/HttpUtils";
import ProductDesc from "./Detail/ProductDesc";

class Detail extends Component {

    state = {
        prodId : '',
    }

    constructor(props){
        super(props);
    }

    componentDidMount(){
        let prodId = this.props.match.params.id;
        var self = this;
        HttpUtils.getProductInfo(prodId , {
            success : function (response) {
                self.setState({product : response.data})
            }
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <SearchBar/>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <TopMenu/>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row>
                            <ProductCrumbs/>
                        </Row>
                        <Row>
                            <Col span={8}>
                                <ImageGallery prodId={this.props.match.params.id}/>
                            </Col>
                            <Col span={15} offset={1}>
                                <ProductInfo prodId={this.props.match.params.id} product={this.state.product}/>
                            </Col>
                        </Row>
                        <Row style={{marginTop:'30px'}}>
                            <Col span={5}>
                                <RecommendedProducts prodId={this.props.match.params.id}/>
                            </Col>
                            <Col span={19}>
                                <Tabs defaultActiveKey="1">
                                    <Tabs.TabPane tab="商品详情" key="1">
                                        <ProductDesc prodId={this.props.match.params.id}/>
                                    </Tabs.TabPane>
                                    <Tabs.TabPane tab="规格参数" key="2">
                                        <AttributesTable prodId={this.props.match.params.id}/>
                                    </Tabs.TabPane>
                                    <Tabs.TabPane tab="评论" key="3">
                                        <CommentListView product={this.state.product}/>
                                    </Tabs.TabPane>
                                </Tabs>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <FooterBar/>
                </Row>
            </div>
        )
    }
}

export default Detail;