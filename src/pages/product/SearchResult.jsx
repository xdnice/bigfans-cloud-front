import React from 'react';

import {Row, Col, Divider, Card , Pagination} from 'antd';
import TopBar from '../../components/TopBar';
import FootBar from 'components/FootBar'
import SearchBar from '../../components/SearchBar'
import TopMenu from '../../components/TopMenu';
import SearchFilters from '../../components/SearchFilters'

import {Link} from 'react-router-dom'

import HttpUtils from 'utils/HttpUtils'

const gridStyle = {
    width: '20%',
    height:'250px',
    textAlign: 'center',
};


class SearchResult extends React.Component {

    state = {
        products: [
        ]
    }

    componentDidMount () {
        console.info(this.props.location.query)
    }

    setSearchResult(result){
        if(!result){
            return;
        }
        this.setState({products : result.products})
        this.setState({pageCount : result.pageCount})
        this.setState({total : result.recordCount})
        this.setState({currentPage : result.currentPage})
    }

    changePage(cp) {
        let page = this.state.currentPage
        if(page == cp){
            return;
        }
        let params = this.props.location.search;
        let newUrl = HttpUtils.changeUrlParam(this.props.location.search , 'cp' , cp)
        window.location.href=newUrl
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <SearchBar/>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <TopMenu/>
                    <Col span={3}/>
                </Row>
                <Row style={{marginTop: '20px'}}>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row>
                            <SearchFilters setSearchResult={(res) => this.setSearchResult(res)}/>
                        </Row>
                        <Row style={{marginTop: '20px'}}>
                            {
                                this.state.products.map((prod, index) => {
                                    return (
                                        <Card.Grid style={gridStyle} key={prod.id}>
                                            <Row>
                                                <Link to={'/product/' + prod.id} target='_blank'>
                                                    <img style={{width: '150px' , height:'150px'}}
                                                         src={prod.imagePath}/>
                                                </Link>
                                            </Row>
                                            <Row>
                                                <Link to={'/product/' + prod.id} target='_blank'>{prod.name}</Link><br/>
                                                <span>{prod.price}</span>
                                            </Row>
                                        </Card.Grid>
                                    )
                                })
                            }
                        </Row>
                        <Row>
                            <div style={{marginTop:'30px'}}>
                            <Pagination className='pull-right' current={this.state.currentPage} total={this.state.total} pageSize={30} onChange={(cp) => this.changePage(cp)} />
                            </div>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <FootBar/>
                </Row>
            </div>
        )
    }
}

export default SearchResult;