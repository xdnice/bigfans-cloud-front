/**
 * Created by lichong on 2/27/2018.
 */
import React from 'react';

import {Row, Col, Card, Icon , Divider} from 'antd';
import TopBar from '../../components/TopBar';
import FootBar from '../../components/FootBar';
import CenterMenu from '../../components/UserCenterMenus'
import HttpUtils from "../../utils/HttpUtils";

class MyAddress extends React.Component {

    state = {
        addresses: [
            {}
        ]
    }

    onCreate = (e) => {
        let currentAddresses = this.state.addresses;

    }

    componentDidMount() {
        var self = this;
        HttpUtils.getMyAddress({}, {
            success: function (response) {
                self.setState({addresses: response.data})
            }
        })
    }

    delete(id) {
        var self = this;
        HttpUtils.deleteMyAddress({}, {
            success: function (response) {
                self.setState({addresses: response.data})
            }
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row gutter={15}>
                            <Col span={5}>
                                <CenterMenu currentMenu="myaddress"/>
                            </Col>
                            <Col span={19}>
                                <div>
                                    {
                                        this.state.addresses.map((address, index) => {
                                            return (
                                                <Card style={{marginTop: '20px'}} key={index}>
                                                    <a href="javascript:void(0)"
                                                       onClick={(e) => this.delete(address.id)}
                                                       className="pull-right"><Icon type="close-square"/></a>
                                                    <Row>
                                                        <Col span={3}>
                                                            <span className="pull-right">收货人</span>
                                                        </Col>
                                                        <Col offset={4}>
                                                            <span className="pull-left">{address.consignee}</span>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col span={3}>
                                                            <span className="pull-right">所属地区</span>
                                                        </Col>
                                                        <Col offset={4}>
                                                            <span
                                                                className="pull-left">{address.province}{address.city}{address.region}</span>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col span={3}>
                                                            <span className="pull-right">地址</span>
                                                        </Col>
                                                        <Col offset={4}>
                                                            <span className="pull-left">{address.detailAddress}</span>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col span={3}>
                                                            <span className="pull-right">手机</span>
                                                        </Col>
                                                        <Col offset={4}>
                                                            <span className="pull-left">{address.mobile}</span>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col span={3}>
                                                            <span className="pull-right">电子邮箱</span>
                                                        </Col>
                                                        <Col offset={4}>
                                                            <span className="pull-left">{address.email}</span>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <div className="pull-right">
                                                            <a href="javascript:void(0)" style={{marginRight: '20px'}}>编辑</a>
                                                            {
                                                                !address.isPrimary
                                                                &&
                                                                <a href="javascript:void(0)">设置为默认</a>
                                                            }
                                                        </div>
                                                    </Row>
                                                </Card>
                                            )
                                        })
                                    }
                                </div>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <FootBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default MyAddress;